using ULSA
using Pkg
using Test
using CSV
using DataFrames
using JLD
#using DecisionTree



#include("/Users/saersamanipour/Desktop/dev/pkgs/ULSA/src/ULSA.jl")

#"""
@testset "Dependencies" begin

    mm=pathof(ULSA)
    path2comp="test_report_comp.csv"
    spec_list = DataFrame(CSV.File(path2comp));


    @test isfile(joinpath(mm[1:end-7],"model.bson")) == 1
    @test isfile(joinpath(mm[1:end-7],"CompTox.csv")) == 1
    @test spec_list.Parent[1] == 0

end


@testset "ULSA.jl" begin

    path2comp="test_report_comp.csv"
    weight_f=[1,1,1,1,1,1,1]
    mode="POSITIVE"
    source="ESI"


    table = featureID_comp(mode,source,path2comp,weight_f)
    table1 = featureMF_comp(mode,source,path2comp)
    r = featureID_comp_batch(mode,source,pwd(),weight_f)

    @test size(table,1) == 128
    @test table.MS1Mass[1] == 83.952275
    #@test table1.Score[3] == 0
    @test r == 1

end
