using CSV
using DataFrames
using Statistics
using Random
using ScikitLearn
using PyCall 
using ScikitLearn.CrossValidation: cross_val_score
using ScikitLearn.CrossValidation: train_test_split


@sk_import ensemble: RandomForestClassifier
jl = pyimport("joblib")

#############################################################################
# Processing function


function pre_process(ID_list,tot_score)

    #prob_mat=zeros(size(ID_list,1),13+m);
    prob_mat=zeros(size(ID_list,1),13);

    for i=1:size(ID_list,1)
        #println(i)
        if isequal(ID_list.Name[i],missing)==0 && ID_list.ID[i] == ID_list.Name[i] &&
             parse.(Float64,split(ID_list.RefMatchFrag[i],"--"))[1] > 0

            prob_mat[i,1]= 1

        end
        f_r=parse.(Float64,split(ID_list.RefMatchFrag[i],"--"))
        prob_mat[i,2]=f_r[1]
        prob_mat[i,3]=f_r[2]
        prob_mat[i,4]=round(100*f_r[1]/f_r[2])

        f_u=parse.(Float64,split(ID_list.UsrMatchFrag[i],"--"))
        prob_mat[i,5]=f_u[1]
        prob_mat[i,6]=f_u[2]
        prob_mat[i,7]=round(100*f_u[1]/f_u[2])

        prob_mat[i,8]=ID_list.MS1Error[i]
        prob_mat[i,9]=ID_list.MS2Error[i]
        prob_mat[i,10]=ID_list.MS2ErrorStd[i]
        prob_mat[i,11]=ID_list.DirectMatch[i]
        prob_mat[i,12]=ID_list.ReversMatch[i]
        prob_mat[i,13]=round(100*ID_list.FinalScore[i]/tot_score)

    end

    return prob_mat

end

######################################################


filenames=["Report_assessment_90.csv","Report_assessment_60.csv","Report_assessment_40.csv","Report_assessment_10.csv"]
path="/Users/saersamanipour/Desktop/dev/ID_model"
tot_score=7

function comb_prob_mat(filenames,path,tot_score)

    #com_prob_mat=zeros(1,1697)
    com_prob_mat=zeros(1,13)

    for i=1:length(filenames)
        println(i)
        filename=joinpath(path,filenames[i])
        ID_list = DataFrame(CSV.File(filename));
        prob_mat = pre_process(ID_list,tot_score);
        com_prob_mat=vcat(com_prob_mat,prob_mat)

    end
    com_prob_mat[2:end,:]

    return com_prob_mat

end

com_prob_mat = comb_prob_mat(filenames,path,tot_score);


##############################



com_prob_mat[isnan.(com_prob_mat) .== 1] .=0;

X_train, X_test, y_train, y_test = train_test_split(com_prob_mat[:,2:end],
com_prob_mat[:,1], test_size=0.10, random_state=42);


clf = RandomForestClassifier(n_estimators=1200, min_samples_leaf=3,verbose =1,
 oob_score =true, max_features= size(X_train,2),n_jobs=6)

fit!(clf, X_train, y_train)

accuracy_cl = score(clf, X_train, y_train) # 0.986
ac_cl = score(clf, X_test, y_test) # 0.967
clf.feature_importances_
fi_c_i = sortperm(clf.feature_importances_, rev=true)
fi_c = 100 .* sort(clf.feature_importances_, rev=true)

clf.predict_proba(X_train)
clf.verbose = 0 

jl.dump(clf,"ConfAssess_clf_model.joblib",compress=5)


##############################
using EvoTrees
using CategoricalArrays

train_ratio = 0.8
train_indices = randperm(size(com_prob_mat,1))[1:Int(round(train_ratio * size(com_prob_mat,1)))]

x_train = com_prob_mat[train_indices,2:end]
y_train = categorical(com_prob_mat[train_indices,1])

x_eval = com_prob_mat[setdiff(1:size(com_prob_mat,1), train_indices),2:end]
y_eval = categorical(com_prob_mat[setdiff(1:size(com_prob_mat,1), train_indices),1])

# 

config = EvoTreeClassifier(nrounds=1200, 
    eta=0.01, 
    max_depth=5, 
    lambda=0.1, 
    rowsample=0.8, 
    colsample=0.9)

model = fit_evotree(config; x_train, y_train, x_eval = x_test, y_eval = y_test, metric = :mlogloss, early_stopping_rounds=100,print_every_n=10)

pred_train = model(x_train)
idx_train = [findmax(row)[2] for row in eachrow(pred_train)]
mean(idx_train .== levelcode.(y_train))

pred_test = model(x_test)
idx_test = [findmax(row)[2] for row in eachrow(pred_test)]
mean(idx_test .== levelcode.(y_test))

EvoTrees.importance(model)
EvoTrees.save(model, "/Users/saersamanipour/Desktop/dev/pkgs/ulsa.jl/training/model.bson")

m = EvoTrees.load("/Users/saersamanipour/Desktop/dev/pkgs/ulsa.jl/training/model.bson")

pred_test_ = m(x_test)
idx_test = [findmax(row)[2] for row in eachrow(pred_test_)]
mean(idx_test .== levelcode.(y_test))