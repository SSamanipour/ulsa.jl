using CSV, DataFrames, Statistics




function constructSuspectlistSeperate(inchikeys, smiles, name, formula, pathDB = "")
    ick = false
    sml = false
    nam = false
    fml = false

    #ready variables
    if length(inchikeys) != 0
        search = "inchikeys"
        it = length(inchikeys)
        if length(smiles) == 0
            smiles = fill("NA", length(inchikeys))
            sml = true
        end
    elseif length(smiles) != 0
        search = "smiles"
        it = length(smiles)
        if length(inchikeys) == 0
            inchikeys = fill("NA", length(smiles))
            ick = true
        end
    else
        println("At least provide inchikeys or smiles")
        return
    end
    if length(name) == 0
        name = fill("NA",length(inchikeys))
        nam = true
    end
    if length(formula) == 0
        formula = fill("NA",length(inchikeys))
        fml = true
    end
   
    #load DB
    DB = CSV.read(pathDB, DataFrame)
    sus = []
   
    for i = 1:it
        # println(i)
        if search == "inchikeys"
            ind = findall(DB[!,"INCHIKEY"] .== inchikeys[i])
        elseif search == "smiles"
            ind = findall(DB[!,"SMILES"] .== smiles[i])
            println(ind)
        else
            println("No selected search option")
            return
        end
        if isempty(ind)
            continue
        end

        for s in ind
            # println(s)
            if isempty(sus)
                sus = DataFrame(INCHIKEY = inchikeys[i], ION_MODE = DB[s,"ION_MODE"], IONIZATION = DB[s,"IONIZATION"], 
                                EXACT_MASS = DB[s,"EXACT_MASS"], FORMULA = formula[i], MZ_VALUES = DB[s,"MZ_VALUES"], MZ_INT = DB[s,"MZ_INT"], 
                                NAME = name[i], DATA_TYPE = DB[s,"DATA_TYPE"], SMILES = smiles[i])
            else
                sus = push!(sus,(inchikeys[i], DB[s,"ION_MODE"], DB[s,"IONIZATION"], DB[s,"EXACT_MASS"], formula[i], DB[s,"MZ_VALUES"], DB[s,"MZ_INT"], 
                            name[i], DB[s,"DATA_TYPE"], smiles[i]), promote = true)
            end
            #fill in others info from DB
            if !ick
                sus[end,"INCHIKEY"] = DB[s,"INCHIKEY"]
            end
            if !sml
                sus[end,"SMILES"] = DB[s,"SMILES"]
            end
            if !nam
                sus[end,"NAME"] = DB[s,"NAME"]
            end
            if !fml
                sus[end,"FORMULA"] = DB[s,"FORMULA"]
            end
        end
    end

    #clean up
    for i = 1:size(sus, 1)
        if sus[i,"MZ_VALUES"][1] .== 'A'
            sus[i,"MZ_VALUES"] = sus[i,"MZ_VALUES"][4:end]
        elseif sus[i,"MZ_VALUES"][1] .== 'F'
            sus[i,"MZ_VALUES"] = sus[i,"MZ_VALUES"][8:end]
        end
        if sus[i,"MZ_INT"][1] .== 'A'
            sus[i,"MZ_INT"] = sus[i,"MZ_INT"][4:end]
        elseif sus[i,"MZ_INT"][1] .== 'F'
            sus[i,"MZ_INT"] = sus[i,"MZ_INT"][8:end]
        end
    end
   
    if isempty(sus)
        println("No matches found")
        return sus
    end

    sus[sus[!,"ION_MODE"] .== "P","ION_MODE"] .= "POSITIVE"
    sus[sus[!,"ION_MODE"] .== "N","ION_MODE"] .= "NEGATIVE"
    return sus
end


function getVec(matStr)
	if matStr[1] .== '['
		if contains(matStr, ", ")
		 	str = split(matStr[2:end-1],", ")
		else
		 	str = split(matStr[2:end-1]," ")
		end
	elseif matStr[1] .== 'A'
		if contains(matStr, ", ")
			str = split(matStr[5:end-1],", ")
		else
			str = split(matStr[5:end-1]," ")
		end
	elseif matStr[1] .== 'F'
		if matStr .== "Float64[]"
			return []
		else
			str = split(matStr[9:end-1],", ")
		end
	else
		println("New vector start")
	end
	if length(str) .== 1 && cmp(str[1],"") .== 0
		return []
	else
		str = parse.(Float64, str)
		return str
	end
end



function getFrags(ind,DB)
    frags = []
    ints = []

    for f in ind
        frags = append!(frags,getVec(DB[f,"MZ_VALUES"]))
        if sum(split(DB[f,"MZ_VALUES"],"") .== ",") == sum(split(DB[f,"MZ_INT"],"") .== ",") 
            ints = append!(ints,getVec(DB[f,"MZ_INT"]))
        else   
            ints = append!(ints, fill(NaN, sum(split(DB[f,"MZ_VALUES"],"") .== ",")+1))
        end
    end

    return frags, ints
end


function fragMerge(frags, ints, massTol)
    #sort from high to low intensity
    sp = sortperm(ints, rev = true)
    frags = frags[sp]
    ints = ints[sp]

    #remove duplicate fragments
    frag = unique(frags)
    int = zeros(length(frag))
    for g = 1:length(frag)
    	ind_int = findall(frag[g] .== frags)
    	int[g] = median(ints[ind_int])
    end

    #sort fragments
    sp = sortperm(frag, rev = true)
    frag = frag[sp]
    int = int[sp]

    return frags, ints
end

function constructSuspectlistMerged(inchikeys, smiles, name, formula, massTol, pathDB = "")
    ick = false
    sml = false
    nam = false
    fml = false

    #ready variables
    if length(inchikeys) != 0
        search = "inchikeys"
        it = length(inchikeys)
        if length(smiles) == 0
            smiles = fill("NA", length(inchikeys))
            sml = true
        end
    elseif length(smiles) != 0
        search = "smiles"
        it = length(smiles)
        if length(inchikeys) == 0
            inchikeys = fill("NA", length(smiles))
            ick = true
        end
    else
        println("At least provide inchikeys or smiles")
        return
    end
    if length(name) == 0
        name = fill("NA",length(inchikeys))
        nam = true
    end
    if length(formula) == 0
        formula = fill("NA",length(inchikeys))
        fml = true
    end
   
    #load DB
    DB = CSV.read(pathDB, DataFrame)
    sus = []
   
    for i = 1:it
        println("it = $i")
        if search == "inchikeys"
            indp = findall((DB[!,"INCHIKEY"] .== inchikeys[i]) .& (DB[!,"ION_MODE"] .== "POSITIVE"))
            indn = findall((DB[!,"INCHIKEY"] .== inchikeys[i]) .& (DB[!,"ION_MODE"] .== "NEGATIVE"))
        elseif search == "smiles"
            indp = findall((DB[!,"SMILES"] .== inchikeys[i]) .& (DB[!,"ION_MODE"] .== "POSITIVE"))
            indn = findall((DB[!,"SMILES"] .== inchikeys[i]) .& (DB[!,"ION_MODE"] .== "NEGATIVE"))
        else
            println("No selected search option")
            return
        end
        
        if (isempty(indp) || isnothing(indp)) && (isempty(indn) || isnothing(indn))
            continue
        end

        #extract all fragments
        if isnothing(indp) ||isempty(indp)
            fragp = []
            intp = Int64[]
        else
            println(indp)
            fragp, intp = getFrags(indp,DB)
            fragp, intp = fragMerge(fragp, intp, massTol)
            if isempty(sus)
                sus = DataFrame(INCHIKEY = inchikeys[i], ION_MODE = DB[indp[1],"ION_MODE"], IONIZATION = DB[indp[1],"IONIZATION"], 
                                EXACT_MASS = DB[indp[1],"EXACT_MASS"], FORMULA = formula[i], MZ_VALUES = string(fragp), MZ_INT = string(intp), 
                                NAME = name[i], DATA_TYPE = DB[indp[1],"DATA_TYPE"], SMILES = smiles[i])
            else
                sus = push!(sus,(inchikeys[i], DB[indp[1],"ION_MODE"], DB[indp[1],"IONIZATION"], DB[indp[1],"EXACT_MASS"], formula[i], string(fragp), string(intp), 
                            name[i], DB[indp[1],"DATA_TYPE"], smiles[i]), promote = true)
            end
            #fill in others info from DB
            if ick
                sus[end,"INCHIKEY"] = DB[indp[1],"INCHIKEY"]
            end
            if sml
                sus[end,"SMILES"] = DB[indp[1],"SMILES"]
            end
            if nam
                sus[end,"NAME"] = DB[indp[1],"NAME"]
            end
            if fml
                sus[end,"FORMULA"] = DB[indp[1],"FORMULA"]
            end
        end
        if isnothing(indn) || isempty(indn)
            fragn = []
            intn = Int64[]
        else
            fragn, intn = getFrags(indn,DB)
	        fragn, intn = fragMerge(fragn, intn, massTol)
            if isempty(sus)
                sus = DataFrame(INCHIKEY = inchikeys[i], ION_MODE = DB[indn[1],"ION_MODE"], IONIZATION = DB[indn[1],"IONIZATION"], 
                                EXACT_MASS = DB[indn[1],"EXACT_MASS"], FORMULA = formula[i], MZ_VALUES = string(fragn), MZ_INT = string(intn), 
                                NAME = name[i], DATA_TYPE = DB[indn[1],"DATA_TYPE"], SMILES = smiles[i])
            else
                sus = push!(sus,(inchikeys[i], DB[indn[1],"ION_MODE"], DB[indn[1],"IONIZATION"], DB[indn[1],"EXACT_MASS"], formula[i], string(fragn), string(intn), 
                            name[i], DB[indn[1],"DATA_TYPE"], smiles[i]), promote = true)
            end
            #fill in others info from DB
            if ick
                sus[end,"INCHIKEY"] = DB[indn[1],"INCHIKEY"]
            end
            if sml
                sus[end,"SMILES"] = DB[indn[1],"SMILES"]
            end
            if nam
                sus[end,"NAME"] = DB[indn[1],"NAME"]
            end
            if fml
                sus[end,"FORMULA"] = DB[indn[1],"FORMULA"]
            end
        end
    end

   
    if isempty(sus)
        println("No matches found")
        return sus
    end

    #clean up
    for i = 1:size(sus, 1)
        if sus[i,"MZ_VALUES"][1] .== 'A'
            sus[i,"MZ_VALUES"] = sus[i,"MZ_VALUES"][4:end]
        elseif sus[i,"MZ_VALUES"][1] .== 'F'
            sus[i,"MZ_VALUES"] = sus[i,"MZ_VALUES"][8:end]
        end
        if sus[i,"MZ_INT"][1] .== 'A'
            sus[i,"MZ_INT"] = sus[i,"MZ_INT"][4:end]
        elseif sus[i,"MZ_INT"][1] .== 'F'
            sus[i,"MZ_INT"] = sus[i,"MZ_INT"][8:end]
        end
    end

    sus[sus[!,"ION_MODE"] .== "P","ION_MODE"] .= "POSITIVE"
    sus[sus[!,"ION_MODE"] .== "N","ION_MODE"] .= "NEGATIVE"
    return sus
end


# pathDB = "C:\\Users\\dherwer\\OneDrive - UvA\\EMCMS\\Mix_files\\Databases\\Database_INTERNAL_2022-11-17.csv"

# pathfile = "C:\\Users\\dherwer\\OneDrive - UvA\\CNL\\Data\\CNL_Ref_susconv.csv"
# info = CSV.read(pathfile, DataFrame)

# inchikeys = info[!,"INCHIKEY"]
# smiles = ""
# name = ""
# formula = ""



# massTol = 0.01  #Da
# sus = constructSuspectlistSeperate(inchikeys, smiles, name, formula, pathDB)

# sus = constructSuspectlistMerged(inchikeys, smiles, name, formula, massTol, pathDB)
#CSV.write("C:\\Users\\gijsb\\Desktop\\SuspectScreening_GUI\\Output_Sus_List.csv",sus)
