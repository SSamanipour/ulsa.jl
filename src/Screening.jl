using DataFrames
using CSV
#using PyPlot
using LinearAlgebra
using Statistics
using ProgressBars
# using Plots
# plotlyjs()

########################################################################
# Conversion the suspect list to suspect entries

function suslist2entries(path2sus)
    sus_l = CSV.File(path2sus) |> DataFrame
    sus_e = Array{Any}(undef,size(sus_l,1),9);
    sus_e[:,1] = 1:size(sus_l,1)
    sus_e[:,2] = sus_l[!,"INCHIKEY"]
    sus_e[:,3] = sus_l[!,"EXACT_MASS"]
    sus_e[:,6] = sus_l[!,"FORMULA"]
    sus_e[:,9] = sus_l[!,"DATA_TYPE"]


    for i in ProgressBar(1:size(sus_l,1), printing_delay=0.1)


        if sus_l[!,"ION_MODE"][i] == "POSITIVE"
            sus_e[i,4] = abs.(1.007825 .+ sus_l[!,"EXACT_MASS"][i])
        elseif sus_l[!,"ION_MODE"][i] == "NEGATIVE"
            sus_e[i,4] = abs.(1.007825 .- sus_l[!,"EXACT_MASS"][i])

        else
            sus_e[i,4] = NaN
        end

        if ismissing(sus_l[!,"NAME"][i]) == 0

            nn = split(sus_l[!,"NAME"][i],"\'")
            if length(nn) > 2
                sus_e[i,5] = nn[2]

            elseif length(nn) == 1
                sus_e[i,5] = nn[1]
            else
                sus_e[i,5] = "NA"

            end
        else
            sus_e[i,5] = "NA"
        end

        if length(sus_l[!,"MZ_VALUES"][i][2:end-1]) > 0

            sus_e[i,7] = parse.(Float64,split(sus_l[!,"MZ_VALUES"][i][2:end-1],","))
            sus_e[i,8] = parse.(Float64,split(sus_l[!,"MZ_INT"][i][2:end-1],","))

        else
            sus_e[i,7] = 0
            sus_e[i,8] = 0
        end


    end

    return sus_e

end


########################################################################
# XIC extract

function xic_extract(mz_v,mz_i,mass,mass_tol)

    md = abs.(mz_v .- mass);
    mz_i1 = deepcopy(mz_i)
    mz_i1[md .>= mass_tol] .=0



    xic = maximum(mz_i1,dims=2)
    xic_s = sum(mz_i1,dims=2)
    return xic, mz_v[mz_i1 .>0], xic_s

end



########################################################################
# XIC ms2 extract

# mz_v2_s_f = mz_v2_s[1]
# mz_i2_s_f = mz_i2_s[1]

function xic_extract_ms2(mz_v2_s_f,mz_i2_s_f,frag,mass_tol)

    md = abs.(mz_v2_s_f .- frag);
    mz_i2s1 = deepcopy(mz_i2_s_f)
    mz_i2s1[md .>= mass_tol] .=0



    xic_ms2 = maximum(mz_i2s1,dims=2)
    return xic_ms2, mz_v2_s_f[ mz_i2s1 .>0]

end



########################################################################
# SWATH sig extract

function swath_extract(mz_v2,mz_i2,mz_t2,mass,pr_i2)

    md = abs.(pr_i2 .- mass);
    mn = minimum(md)
    mz_v2_s = mz_v2[md .== mn,:];
    mz_i2_s = mz_i2[md .== mn,:];
    mz_t2 = mz_t2[md .== mn];

    return(mz_v2_s,mz_i2_s, mz_t2)

end

########################################################################
# Match isotopic pattern mass diff method

function iso_match(ms_x,ms_y,mass,iso_depth,mass_tol)

    iso_mass = (([0,1,2,3,4,5,6,7,8,9,10] .* 1.0033) .+ mass)[1:iso_depth]

    iso_m = zeros(iso_depth)
    iso_mz = zeros(iso_depth)

    for i=1:length(iso_mass)
        #println(i)
        tv1 = abs.(iso_mass[i] .- ms_x)
        ms_x_s = ms_x[tv1 .<= mass_tol]
        ms_y_s = ms_y[tv1 .<= mass_tol]

        if length(ms_x_s) ==0
            break
        end

        if length(ms_x_s) >=3
            p = findpeaks( ms_y_s,  ms_x_s, 0, mass_tol/2)
        else
            p = findfirst(tv1 .<= mass_tol)
        end 
        

        if length(p) ==0
            break
        end

        if length(p) >1
            iso_m[i] =1
            iso_mz[i] = ms_x_s[p[1]]
        elseif length(p) == 1
            iso_m[i] =1
            iso_mz[i] = ms_x_s[argmax(ms_y_s)]

        end

    end

    iso_depth_match = sum(iso_m)

    return(iso_depth_match,iso_mz[iso_mz .>0])

end


########################################################################
# Match fragments

# frags_m,frags_mi = frag_match(mz_v2,mz_i2,mz_t2,indt,mass_tol,rt_w,rt_w_ind,frags)
# mz_v2_s = deepcopy(mz_v2)
# mz_i2_s = deepcopy(mz_i2)

function frag_match(mz_v2_s,mz_i2_s,mz_t2,indt,mass_tol,rt_w,rt_w_ind,frags)

    indr = findall((mz_t2 .>= indt - rt_w) .& (mz_t2 .<= indt +rt_w))
    if isempty(indr)
        println("No MS2 information in time range of suspect compound")
        return [], []
    end
    lb = minimum(indr)
    ub = maximum(indr)

    ind = argmin(abs.(mz_t2 .- indt))

    # if indt - rt_w > 0 && indt + rt_w <= size(mz_t2,1)
    #     lb = indt - rt_w
    #     ub = indt + rt_w
    # elseif indt - rt_w <= 0
    #     lb = 1
    #     ub = indt + rt_w
    # elseif indt +rt_w > size(mz_v2_s,1)
    #     lb = indt - rt_w
    #     ub = size(mz_v2_s,1)
    # end

    frags_m = zeros(length(frags))
    frags_i = zeros(length(frags))


    for i =1:length(frags)
        #println(frags[i])
        # stem(mz_v2_s[ind,:],mz_i2_s[ind,:]);
        tv = mz_v2_s[ind,:];
        tv[abs.(tv .- frags[i]) .> mass_tol] .=0
        mz_vec = mz_v2_s[ind, tv .>0]
        int_vec = mz_i2_s[ind, tv .>0]
        # plot(mz_vec,int_vec)

        if length(mz_vec) == 0
            continue
    
        end

        m_pf = argmax(int_vec)

        if abs(mz_vec[m_pf] - frags[i]) >= mass_tol/4
            continue
        end
        # PyPlot.scatter(mz_v2_s[ind,:],mz_i2_s[ind,:])
        xic, mz, xic_s = xic_extract(mz_v2_s[lb:ub,:],mz_i2_s[lb:ub,:],frags[i],mass_tol)
        # plot(xic)
        #xic, mz = xic_extract_ms2(mz_v2_s,mz_i2_s,frags[i],mass_tol)
        if length(xic) == 0
            continue
        end
        pf = findpeaks(xic[:],lb:ub,0,rt_w_ind)
        if size(pf,1) >= 1 && abs(pf[1] - rt_w_ind) <= rt_w_ind/10
            #println(frags[i])
            frags_m[i] = frags[i]
            frags_i[i] = xic[pf[1]]

        end
    end

    return(frags_m,frags_i)

end



########################################################################
# Match fragments

function frag_match_mc(mz_v2_s,mz_i2_s,ind_m1,mass_tol,rt_w,frags)


    frags_m = zeros(length(frags))
    frags_i = zeros(length(frags))

    # plot(mz_v2_s[2][ind_m1,:],mz_i2_s[2][ind_m1,:])

    for i =1:length(frags)
        #println(frags[i])
        for j=1:length(mz_i2_s)
            if frags_m[i] > 0
                continue
            end

            tv = mz_v2_s[j][ind_m1,:];
            tv[abs.(tv .- frags[i]) .> mass_tol] .=0
            mz_vec = mz_v2_s[j][ind_m1, tv .>0]
            int_vec = mz_i2_s[j][ind_m1, tv .>0]

            if length(mz_vec) < 3
                continue
            end

            m_pf = argmax(int_vec)

            if abs(mz_vec[m_pf] - frags[i]) >= mass_tol/4
                continue
            end

            xic, mz, xic_s = xic_extract(mz_v2_s[j],mz_i2_s[j],frags[i],mass_tol)

            if length(xic) == 0
                continue
            end
            pf = findpeaks(xic[:],1:length(xic), 0,rt_w)
            if size(pf,1) >= 1 && abs(pf[1] - rt_w) <= round(rt_w/8)
                frags_m[i] = frags[i]
                frags_i[i] = xic[pf[1]]

            end

        end
        #xic, mz = xic_extract_ms2(mz_v2_s,mz_i2_s,frags[i],mass_tol)



    end

    return(frags_m,frags_i)

end


########################################################################
# Single suspect matching SWATH

function ss_swath(chrom,sus,mass_tol,min_int,rt_width,iso_depth)

    min_mz = minimum(chrom["MS2"]["Mz_values"][chrom["MS2"]["Mz_values"] .>0])

    mz_v = chrom["MS1"]["Mz_values"];
    mz_i = chrom["MS1"]["Mz_intensity"];
    mass = sus[4]
    frags = sus[7][sus[7] .>= min_mz]
    frags_i = sqrt.(sus[8][sus[7] .>= min_mz] ./ sum(sus[8][sus[7] .>= min_mz]))

    xic, mz, xic_s = xic_extract(mz_v,mz_i,mass,mass_tol)
    med = median(xic)

    if med - min_int <=0

        peaks = findpeaks(xic[:], chrom["MS1"]["Rt"][:], min_int ,rt_width)
    else
        peaks = findpeaks(bc(xic)[:] ,chrom["MS1"]["Rt"][:], min_int ,rt_width)

    end

    if length(peaks) == 0
        return rep_f = zeros(1,14)
    end

    # plot(chrom["MS1"]["Rt"][:], xic, title="Prominent peaks")
    # scatter!(chrom["MS1"]["Rt"][peaks], xic[peaks])

    rep = Array{Any}(undef,length(peaks),14)
    rep[:,1] .= sus[5]
    rep[:,2] .= sus[2]
    rep[:,3] .= sus[6]

    mz_v2 = chrom["MS2"]["Mz_values"];
    mz_i2 = chrom["MS2"]["Mz_intensity"];
    mz_t2 = chrom["MS2"]["Rt"];
    pr_i2 = chrom["MS2"]["PrecursorIon"];


    mz_v2_s,mz_i2_s, mz_t2 = swath_extract(mz_v2,mz_i2,mz_t2,mass,pr_i2)



    # i=2

    for i=1:length(peaks)
        #println(i)
        ms_x = mz_v[peaks[i],:]
        ms_y = mz_i[peaks[i],:]
        # bar(ms_x,ms_y)
        iso_depth_match,iso_mz = iso_match(ms_x,ms_y,mass,iso_depth,mass_tol)

        if length(iso_mz)==0
            rep[i,:] .= 0
            continue
        end


        ind = peaks[i]
        indt = chrom["MS1"]["Rt"][peaks[i]]
        rt_w = rt_width
        rt_w_ind = argmin(abs.((chrom["MS1"]["Rt"][ind] + rt_width) .- chrom["MS1"]["Rt"][:])) - ind
        frags_m,frags_mi = frag_match(mz_v2_s,mz_i2_s,mz_t2,indt,mass_tol,rt_w,rt_w_ind,frags)

        #ind = peaks[i]
        #rt_w = argmin(abs.((chrom["MS1"]["Rt"][ind] + rt_width) .- chrom["MS1"]["Rt"][:])) - ind

        #frags_m,frags_mi = frag_match(mz_v2_s,mz_i2_s,ind,mass_tol,rt_w,frags)
        mff = round(dot(frags_i, sqrt.(frags_mi ./ sum(frags_mi))),digits=2)

        rep[i,5] = iso_mz[1]
        rep[i,4] = chrom["MS1"]["Rt"][ind]
        rep[i,6] = xic[ind]
        rep[i,7] = iso_depth_match -1
        rep[i,8] = iso_mz[2:end]
        rep[i,9] = length(frags_m[frags_m .>0])
        rep[i,10] = length(frags_m)
        rep[i,11] = mff
        rep[i,12] = frags_m[frags_m .>0]
        rep[i,13] = frags_mi[frags_m .>0]
        rep[i,14] = sus[9]

    end

    rep_f = rep[rep[:,4] .>0,:]

    return rep_f

end




########################################################################
# Screening SWATH
# i=58
# sus_list = sus_e

function screening_swath(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

    rep_final = zeros(1,14)

    for i in ProgressBar(1:size(sus_list,1), printing_delay=0.1)
    #for i=1:size(sus_list,1)
    #    println(i)
        # sus_list = sus_e
        sus = sus_list[i,:]
        if sus[4] - sus[3] > 0
            m = "POSITIVE"
        else
            m = "NEGATIVE"

        end

        if m != mode
            continue
        end

        if length(sus[7]) <=1 && sus[7] ==0
            continue
        end

        res = []

        try
            res = ss_swath(chrom,sus,mass_tol,min_int,rt_width,iso_depth)
        catch

            println("There is something wrong with suspect $i.")

        end
        if length(res)==0 || res[1] == 0
            continue
        end
        rep_final = vcat(rep_final,res)

    end

    rep_final = hcat(zeros(size(rep_final,1),1),rep_final)

    table=DataFrame(rep_final[2:end,:],[:Nr,:Name,:INCHIKEY,:Formula,:Rt,
    :MeasMass,:Int,:IsoDepth,:Isotop,:NrMatchedFrags,:NrTestedFrags,:MatchFactor,:MatchedFrags,:MatchedFragInt,:SpectraType])
    sort!(table,([:MeasMass,:Rt]))
    table[!,"Nr"] = 1:size(table,1)

    return table


end

########################################################################
# DDA sig extract

function dda_extract(mz_v2,mz_i2,mass,pr_i2,rt_ms2,rt_win)

    if rt_win[1] <0
        rt_win[1] = 0
    elseif rt_win[2] > rt_ms2[end]
        rt_win[2] = rt_ms2[end]
    end

    lb =  argmin(abs.(rt_win[1] .- rt_ms2))
    ub = argmin(abs.(rt_win[2] .- rt_ms2))

    md = abs.(pr_i2[lb:ub] .- mass);
    mn = minimum(md)
    if mn > 0.1
        mz_v2_s = []
        mz_i2_s = []
        return(mz_v2_s,mz_i2_s)
    end

    mz_v2_s = mz_v2[lb + argmin(md) - 1,:];
    mz_i2_s = mz_i2[lb + argmin(md) - 1,:];

    return(mz_v2_s,mz_i2_s)

end


########################################################################
# Match fragments DDA

function frag_match_dda(mz_v2_s,mz_i2_s,mass_tol,frags)

    frags_m = zeros(length(frags))
    frags_i = zeros(length(frags))

    for i =1:length(frags)
        #println(i)
        dm = abs.(mz_v2_s .- frags[i])
        if length(mz_i2_s[dm .<= mass_tol]) == 0
            continue
        end

        pf = findpeaks(mz_i2_s[dm .<= mass_tol],mz_v2_s[dm .<= mass_tol], 0 ,mass_tol)
        
        if size(pf,1) >= 1
            frags_m[i] = frags[i]
            frags_i[i] = mz_i2_s[dm .<= mass_tol][pf[1]]

        end
    end



    return(frags_m,frags_i)

end

########################################################################
# Cummulative mean

function cummean(y)
    cm = zeros(size(y))
    cm[1] = y[1]

    for i=2:size(y,1)
        cm[i] = mean(y[1:i])

    end

    return cm
end


########################################################################
# Baseline correction

function bc(xic)

    xic_bc = xic .- cummean(xic)

    xic_bc[xic_bc .< 0] .=0;

    return xic_bc

end


########################################################################
# Single suspect matching DDA

function ss_dda(chrom,sus,mass_tol,min_int,rt_width,iso_depth)

    min_mz = minimum(chrom["MS2"]["Mz_values"][chrom["MS2"]["Mz_values"] .>0])

    mz_v = chrom["MS1"]["Mz_values"];
    mz_i = chrom["MS1"]["Mz_intensity"];
    mass = sus[4]
    frags = sus[7][sus[7] .>= min_mz]
    frags_i = sqrt.(sus[8][sus[7] .>= min_mz] ./ sum(sus[8][sus[7] .>= min_mz]))

    xic, mz, xic_s = xic_extract(mz_v,mz_i,mass,mass_tol)
    med = median(xic)

    if med - min_int <=0

        peaks = findpeaks(xic[:], chrom["MS1"]["Rt"][:], min_int , rt_width)
    else
        peaks = findpeaks(bc(xic)[:] , chrom["MS1"]["Rt"][:], med , rt_width)

    end

    if length(peaks) == 0
        return rep_f = zeros(1,13)
    end

    # plot(chrom["MS1"]["Rt"][:], xic, title="Prominent peaks")
    #scatter!(chrom["MS1"]["Rt"][peaks], xic[peaks])

    rep = Array{Any}(undef,length(peaks),14)
    rep[:,1] .= sus[5]
    rep[:,2] .= sus[2]
    rep[:,3] .= sus[6]

    mz_v2 = chrom["MS2"]["Mz_values"];
    mz_i2 = chrom["MS2"]["Mz_intensity"];
    pr_i2 = chrom["MS2"]["PrecursorIon"];
    rt_ms2 = chrom["MS2"]["Rt"];



    for i=1:length(peaks)
        #println(i)
        ms_x = mz_v[peaks[i],:]
        ms_y = mz_i[peaks[i],:]
        iso_depth_match,iso_mz = iso_match(ms_x,ms_y,mass,iso_depth,mass_tol)

        if length(iso_mz)==0
            rep[i,:] .= 0
            continue
        end


        ind = peaks[i]
        rt_w = argmin(abs.((chrom["MS1"]["Rt"][ind] + rt_width) .- chrom["MS1"]["Rt"][:])) - ind
        rt_win = [0.0,0.0]
        if ind - rt_w <= 0
            rt_win[1] = 0
            rt_win[2] = chrom["MS1"]["Rt"][ind + rt_w]

        elseif ind + rt_w > length(chrom["MS1"]["Rt"])
            rt_win[1] = chrom["MS1"]["Rt"][ind - rt_w]
            rt_win[2] = chrom["MS1"]["Rt"][end]

        else

            rt_win = [chrom["MS1"]["Rt"][ind - rt_w],chrom["MS1"]["Rt"][ind + rt_w]]
        end

        mz_v2_s,mz_i2_s = dda_extract(mz_v2,mz_i2,mass,pr_i2,rt_ms2,rt_win)

        if length(mz_v2_s)==0
            rep[i,5] = iso_mz[1]
            rep[i,4] = chrom["MS1"]["Rt"][ind]
            rep[i,6] = xic[ind]
            rep[i,7] = iso_depth_match -1
            rep[i,8] = iso_mz[2:end]
            rep[i,9] = 0
            rep[i,10] = length(frags)
            rep[i,11] = 0
            rep[i,12] = 0
            rep[i,13] = 0
            rep[i,14] = sus[9]
            continue
        end

        frags_m,frags_mi = frag_match_dda(mz_v2_s,mz_i2_s,mass_tol,frags)
        mff = round(dot(frags_i, sqrt.(frags_mi ./ sum(frags_mi))),digits=2)

        rep[i,5] = iso_mz[1]
        rep[i,4] = chrom["MS1"]["Rt"][ind]
        rep[i,6] = xic[ind]
        rep[i,7] = iso_depth_match -1
        rep[i,8] = iso_mz[2:end]
        rep[i,9] = length(frags_m[frags_m .>0])
        rep[i,10] = length(frags_m)
        rep[i,11] = mff
        rep[i,12] = frags_m[frags_m .>0]
        rep[i,13] = frags_mi[frags_m .>0]
        rep[i,14] = sus[9]

    end

    rep_f = rep[rep[:,4] .>0,:]

    return rep_f

end



########################################################################
# Screening DDA

function screening_dda(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

    rep_final = zeros(1,14)

    for i in ProgressBar(1:size(sus_list,1), printing_delay=0.1)
        #println(i)
        sus = sus_list[i,:]

        if sus[4] - sus[3] > 0
            m = "POSITIVE"
        else
            m = "NEGATIVE"

        end

        if m != mode
            continue
        end

        if length(sus[7]) <=1 && sus[7] ==0
            continue
        end
        res = ss_dda(chrom,sus,mass_tol,min_int,rt_width,iso_depth)
        if length(res)==0 || res[1] == 0
            continue
        end
        rep_final = vcat(rep_final,res)

    end

    rep_final = hcat(zeros(size(rep_final,1),1),rep_final)

    table=DataFrame(rep_final[2:end,:],[:Nr,:Name,:INCHIKEY,:Formula,:Rt,
    :MeasMass,:Int,:IsoDepth,:Isotop,:NrMatchedFrags,:NrTestedFrags,:MatchFactor,:MatchedFrags,:MatchedFragInt,:SpectraType])
    sort!(table,([:MeasMass,:Rt]))
    table[!,"Nr"] = 1:size(table,1)

    return table


end


########################################################################
# Single suspect matching DIA

function ss_dia(chrom,sus,mass_tol,min_int,rt_width,iso_depth)

    min_mz = minimum(chrom["MS2"]["Mz_values"][chrom["MS2"]["Mz_values"] .>0])

    mz_v = chrom["MS1"]["Mz_values"];
    mz_i = chrom["MS1"]["Mz_intensity"];
    mass = sus[4]
    frags = sus[7][sus[7] .>= min_mz]
    frags_i = sqrt.(sus[8][sus[7] .>= min_mz] ./ sum(sus[8][sus[7] .>= min_mz]))

    xic, mz, xic_s = xic_extract(mz_v,mz_i,mass,mass_tol)
    # plot(xic)

    med = median(xic)

    if med - min_int <=0

        peaks = findpeaks(xic[:], chrom["MS1"]["Rt"][:], min_int , rt_width)
    else
        peaks = findpeaks(bc(xic)[:] , chrom["MS1"]["Rt"][:], med , rt_width)

    end

    if length(peaks) == 0
        return rep_f = zeros(1,13)
    end

    #plot(chrom["MS1"]["Rt"][:], xic, title="Prominent peaks")
    #scatter!(chrom["MS1"]["Rt"][peaks], xic[peaks])

    rep = Array{Any}(undef,length(peaks),14)
    rep[:,1] .= sus[5]
    rep[:,2] .= sus[2]
    rep[:,3] .= sus[6]

    mz_v2 = chrom["MS2"]["Mz_values"];
    mz_i2 = chrom["MS2"]["Mz_intensity"];
    mz_t2 = chrom["MS2"]["Rt"]


    for i=1:length(peaks)
        #println(i)
        ms_x = mz_v[peaks[i],:]
        ms_y = mz_i[peaks[i],:]
        # bar(ms_x,ms_y)
        iso_depth_match,iso_mz = iso_match(ms_x,ms_y,mass,iso_depth,mass_tol)

        if length(iso_mz)==0
            rep[i,:] .= 0
            continue
        end

#=         if length(iso_mz)==0
            rep[i,5] = 0

        else
            rep[i,5] = iso_mz[1]
            #continue
        end =#

        ind = peaks[i]
        indt = chrom["MS1"]["Rt"][peaks[i]]
        rt_w = rt_width
        rt_w_ind = argmin(abs.((chrom["MS1"]["Rt"][ind] + rt_width) .- chrom["MS1"]["Rt"][:])) - ind
        frags_m,frags_mi = frag_match(mz_v2,mz_i2,mz_t2,indt,mass_tol,rt_w,rt_w_ind,frags)
        mff = round(dot(frags_i, sqrt.(frags_mi ./ sum(frags_mi))),digits=2)

        rep[i,5] = iso_mz[1]
        rep[i,4] = chrom["MS1"]["Rt"][ind]
        rep[i,6] = xic[ind]
        rep[i,7] = iso_depth_match -1
        rep[i,8] = iso_mz[2:end]
        rep[i,9] = length(frags_m[frags_m .>0])
        rep[i,10] = length(frags_m)
        rep[i,11] = mff
        rep[i,12] = frags_m[frags_m .>0]
        rep[i,13] = frags_mi[frags_m .>0]
        rep[i,14] = sus[9]

    end

    rep_f = rep[rep[:,4] .>0,:]

    return rep_f

end


########################################################################
# Screening DIA

function screening_dia(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

    rep_final = zeros(1,14)

    for i in ProgressBar(1:size(sus_list,1), printing_delay=0.1)
        #println(i)
        sus = sus_list[i,:]

        if sus[4] - sus[3] > 0
            m = "POSITIVE"
        else
            m = "NEGATIVE"

        end

        if m != mode
            continue
        end

        if length(sus[7]) <=1 && sus[7] ==0
            continue
        end
        res = ss_dia(chrom,sus,mass_tol,min_int,rt_width,iso_depth)

        if length(res)==0 || res[1] == 0
            continue
        end
        rep_final = vcat(rep_final,res)

    end

    rep_final = hcat(zeros(size(rep_final,1),1),rep_final)

    table=DataFrame(rep_final[2:end,:],[:Nr,:Name,:INCHIKEY,:Formula,:Rt,
    :MeasMass,:Int,:IsoDepth,:Isotop,:NrMatchedFrags,:NrTestedFrags,:MatchFactor,:MatchedFrags,:MatchedFragInt,:SpectraType])
    sort!(table,([:MeasMass,:Rt]))
    table[!,"Nr"] = 1:size(table,1)

    return table


end


########################################################################
# MC signal extract

function mc_sig_extract(chrom,ind,rt_w)

    ls = length(chrom["MS1"]["Rt"])

    if ind + rt_w <= ls && ind - rt_w >0
        rt_s = chrom["MS1"]["Rt"][ind - rt_w]
        rt_e = chrom["MS1"]["Rt"][ind + rt_w]
        m1_s = 2*rt_w +1

        lb_m2 = findfirst(x -> x >= rt_s,chrom["MS2"]["Rt"])
        ub_m2 = findlast(x -> x <= rt_e,chrom["MS2"]["Rt"])

        c = round((ub_m2 - lb_m2)/m1_s)
        ind_m1 = rt_w+ 1

    elseif ind + rt_w > ls
        rt_s = chrom["MS1"]["Rt"][ind - rt_w]
        rt_e = chrom["MS1"]["Rt"][end]
        m1_s = length(chrom["MS1"]["Rt"][ind - rt_w:end])

        lb_m2 = findfirst(x -> x >= rt_s,chrom["MS2"]["Rt"])
        ub_m2 = findlast(x -> x <= rt_e,chrom["MS2"]["Rt"])

        c = round((ub_m2 - lb_m2)/m1_s)

        ind_m1 = rt_w+ 1

    elseif ind - rt_w <=0
        rt_s = chrom["MS1"]["Rt"][1]
        rt_e = chrom["MS1"]["Rt"][ind + rt_w]
        m1_s = length(chrom["MS1"]["Rt"][1:ind + rt_w])

        lb_m2 = findfirst(x -> x >= rt_s,chrom["MS2"]["Rt"])
        ub_m2 = findlast(x -> x <= rt_e,chrom["MS2"]["Rt"])

        c = round((ub_m2 - lb_m2)/m1_s)
        ind_m1 = ind

    end

    mz_v2_s = Array{Any}(undef,Int(c),1)
    mz_i2_s = Array{Any}(undef,Int(c),1)

    for i =1:Int(c)
        mz_v2_s[i] = chrom["MS2"]["Mz_values"][Int.(lb_m2+i:c:ub_m2),:]
        mz_i2_s[i] = chrom["MS2"]["Mz_intensity"][Int.(lb_m2+i:c:ub_m2),:]

    end
    return mz_v2_s, mz_i2_s, ind_m1

end


########################################################################
# Single suspect matching MC

function ss_mc(chrom,sus,mass_tol,min_int,rt_width,iso_depth)

    min_mz = minimum(chrom["MS2"]["Mz_values"][chrom["MS2"]["Mz_values"] .>0])

    mz_v = chrom["MS1"]["Mz_values"];
    mz_i = chrom["MS1"]["Mz_intensity"];
    mass = sus[4]
    frags = sus[7][sus[7] .>= min_mz]
    frags_i = sqrt.(sus[8][sus[7] .>= min_mz] ./ sum(sus[8][sus[7] .>= min_mz]))

    xic, mz, xic_s = xic_extract(mz_v,mz_i,mass,mass_tol)

    # plot(xic)

    med = median(xic)

    if med - min_int <=0

        peaks = findpeaks(xic[:], chrom["MS1"]["Rt"][:], min_int , rt_width)
    else
        peaks = findpeaks(bc(xic)[:] , chrom["MS1"]["Rt"][:], med , rt_width)

    end

    if length(peaks) == 0
        return rep_f = zeros(1,14)
    end

    #plot(chrom["MS1"]["Rt"][:], xic, title="Prominent peaks")
    #scatter!(chrom["MS1"]["Rt"][peaks], xic[peaks])

    rep = Array{Any}(undef,length(peaks),14)
    rep[:,1] .= sus[5]
    rep[:,2] .= sus[2]
    rep[:,3] .= sus[6]



    for i=1:length(peaks)
        #println(i)
        ms_x = mz_v[peaks[i],:]
        ms_y = mz_i[peaks[i],:]
        iso_depth_match,iso_mz = iso_match(ms_x,ms_y,mass,iso_depth,mass_tol)

        if length(iso_mz)==0
            rep[i,:] .= 0
            continue
        end


        ind = peaks[i]
        rt_w = argmin(abs.((chrom["MS1"]["Rt"][ind] + rt_width) .- chrom["MS1"]["Rt"][:])) - ind

        mz_v2_s,mz_i2_s,ind_m1 = mc_sig_extract(chrom,ind,rt_w)
        frags_m,frags_mi = frag_match_mc(mz_v2_s,mz_i2_s,ind_m1,mass_tol,rt_w,frags)

        mff = round(dot(frags_i, sqrt.(frags_mi ./ sum(frags_mi))),digits=2)

        rep[i,5] = iso_mz[1]
        rep[i,4] = chrom["MS1"]["Rt"][ind]
        rep[i,6] = xic[ind]
        rep[i,7] = iso_depth_match -1
        rep[i,8] = iso_mz[2:end]
        rep[i,9] = length(frags_m[frags_m .>0])
        rep[i,10] = length(frags_m)
        rep[i,11] = mff
        rep[i,12] = frags_m[frags_m .>0]
        rep[i,13] = frags_mi[frags_m .>0]
        rep[i,14] = sus[9]

    end

    rep_f = rep[rep[:,4] .>0,:]

    return rep_f

end


########################################################################
# Screening DIA mc

function screening_mc(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

    rep_final = zeros(1,14)

    for i in ProgressBar(1:size(sus_list,1), printing_delay=0.1)
        #println(i)
        sus = sus_list[i,:]

        if sus[4] - sus[3] > 0
            m = "POSITIVE"
        else
            m = "NEGATIVE"

        end

        if m != mode
            continue
        end

        if length(sus[7]) <=1 && sus[7] ==0
            continue
        end
        res = ss_mc(chrom,sus,mass_tol,min_int,rt_width,iso_depth)
        if length(res)==0 || res[1] == 0
            continue
        end
        rep_final = vcat(rep_final,res)

    end

    rep_final = hcat(zeros(size(rep_final,1),1),rep_final)

    table=DataFrame(rep_final[2:end,:],[:Nr,:Name,:INCHIKEY,:Formula,:Rt,
    :MeasMass,:Int,:IsoDepth,:Isotop,:NrMatchedFrags,:NrTestedFrags,:MatchFactor,:MatchedFrags,:MatchedFragInt,:SpectraType])
    sort!(table,([:MeasMass,:Rt]))
    table[!,"Nr"] = 1:size(table,1)

    return table


end


########################################################################
# Single suspect matching MS1

function s_ms1(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)

    mz_v = chrom["MS1"]["Mz_values"];
    mz_i = chrom["MS1"]["Mz_intensity"];

    xic, mz, xic_s = xic_extract(mz_v,mz_i,sus_e[4],mass_tol)

    med = median(xic)
    mass = sus_e[4]
    cm = cummean(xic)

    if med - min_int <=0

        peaks = findpeaks(xic[:], chrom["MS1"]["Rt"][:], min_int , rt_width)
    else
        peaks = findpeaks(bc(xic)[:] , chrom["MS1"]["Rt"][:], med , rt_width)

    end


    if length(peaks) == 0

        return rep_f =[ ]
    end

    #plot(chrom["MS1"]["Rt"][:], xic, title="Prominent peaks")
    #scatter!(chrom["MS1"]["Rt"][peaks], xic[peaks])

    rep = Array{Any}(undef,length(peaks),9)
    rep[:,1] .= sus_e[5]
    rep[:,2] .= sus_e[2]
    rep[:,3] .= sus_e[6]



    for i=1:length(peaks)
        #println(i)
        ms_x = mz_v[peaks[i],:]
        ms_y = mz_i[peaks[i],:]
        iso_depth_match,iso_mz = iso_match(ms_x,ms_y,mass,iso_depth,mass_tol)

        if length(iso_mz)==0
            rep[i,:] .= 0
            continue
        end


        ind = peaks[i]

        rep[i,5] = iso_mz[1]
        rep[i,4] = chrom["MS1"]["Rt"][ind]
        rep[i,6] = xic[ind]
        rep[i,7] = iso_depth_match -1
        rep[i,8] = iso_mz[2:end]
        rep[i,9] = sus_e[9]

    end

    rep_f = rep[rep[:,4] .>0,:]
    return rep_f

end



########################################################################
# Screening DIA

function screening_ms1(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth,mode)

    rep_final = zeros(1,9)

    for i in ProgressBar(1:size(sus_e,1), printing_delay=0.1)
        #println(i)
        sus_l = sus_e[i,:]

        if sus_l[4] - sus_l[3] > 0
            m = "POSITIVE"
        else
            m = "NEGATIVE"

        end

        if m != mode
            continue
        end

        res = s_ms1(chrom,sus_l,mass_tol,min_int,rt_width,iso_depth)
        if length(res)==0 || res[1] == 0
            continue
        else
            rep_final = vcat(rep_final,res)
        end


    end

    rep_final = hcat(zeros(size(rep_final,1),1),rep_final)

    table=DataFrame(rep_final[2:end,:],[:Nr,:Name,:INCHIKEY,:Formula,:Rt,:MeasMass,:Int,:IsoDepth,:Isotop,:SpectraType])
    table[!,"Nr"] = 1:size(table,1)

    return table


end

########################################################################
# Single suspect matching MS1 proteine

function proteine_s_ms1(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)

    mz_v = chrom["MS1"]["Mz_values"];
    mz_i = chrom["MS1"]["Mz_intensity"];

    xic, mz, xic_s = xic_extract(mz_v,mz_i,sus_e[1],mass_tol)

    med = median(xic)

    cm = cummean(xic)

    if med - min_int <=0

        peaks = findpeaks(xic[:], chrom["MS1"]["Rt"][:], min_int , rt_width)
    else
        peaks = findpeaks(bc(xic)[:] , chrom["MS1"]["Rt"][:], med , rt_width)

    end


    if length(peaks) == 0
        rep_f = zeros(1,12)
        rep_f[:,1] .= sus_e[1]
        rep_f[:,2] .=  sus_e[1] - mass_tol
        rep_f[:,3] .=  sus_e[1] + mass_tol
        rep_f[:,4] .=  sus_e[2]
        rep_f[:,5] .=  sus_e[3]

        return rep_f
    end

    #plot(chrom["MS1"]["Rt"][:], xic, title="Prominent peaks")
    #scatter!(chrom["MS1"]["Rt"][peaks], xic[peaks])

    rep_f = Array{Any}(undef,1,12)
    rep_f[1] = round(mean(mz),digits=4)
    rep_f[2] =  minimum(mz)
    rep_f[3] =  maximum(mz)
    rep_f[4] =  sus_e[2]
    rep_f[5] =  sus_e[3]

    if minimum(abs.(argmax(xic)[1] .- peaks)) == 0
        inn = argmax(xic)[1]
        rep_f[6] = chrom["MS1"]["Rt"][inn]
        ms_x = mz_v[inn,:]
        ms_y = mz_i[inn,:]
        iso_depth_match,iso_mz = iso_match(ms_x,ms_y,sus_e[1],iso_depth,mass_tol)
        p_s = findlast(x -> x >= cm[inn],xic[1:inn]) - 1
        p_e = findfirst(x -> x <= cm[inn],xic[inn:end]) + p_s - 1


        rep_f[7] = chrom["MS1"]["Rt"][p_s]
        rep_f[8] = chrom["MS1"]["Rt"][p_e]
        rep_f[9] = xic[inn]
        rep_f[10] = xic_s[inn]
        rep_f[11] = iso_depth_match -1
        rep_f[12] = iso_mz[2:end]

    end

    return rep_f

end



########################################################################
# Screening DIA proteine

function proteine_screening_ms1(chrom,sus_l,mass_tol,min_int,rt_width,iso_depth)

    rep_final = zeros(1,12)

    for i =1:size(sus_l,1)
        #println(i)
        sus_e = zeros(3)

        sus_e[1] = sus_l[!,"m/z"][i]
        sus_e[2] = sus_l[!,"charge (+)"][i]
        sus_e[3] = sus_l[!,"mass"][i]

        res = proteine_s_ms1(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)
        if length(res)==0 || res[1] == 0
            rep_final = vcat(rep_final,zeros(1,12))
        else
            rep_final = vcat(rep_final,res)
        end


    end

    rep_final = hcat(zeros(size(rep_final,1),1),rep_final)

    table=DataFrame(rep_final[2:end,:],[:Nr,:MeasMz,:MinMz,:MaxMz,:Charge,
    :Mass,:Rt,:RtS,:RtE,:IntMax,:IntSum,:IsoDepth,:Iso])
    #sort!(table,([:MeasMz,:Rt]))
    table[!,"Nr"] = 1:size(table,1)

    return table


end





#############################################################################
# Wrapping function for componentization all files


function suspect_screening(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth)

    if chrom["MS1"]["Polarity"][1] == "+"
        mode="POSITIVE"
    elseif chrom["MS1"]["Polarity"][1] == "-"
        mode="NEGATIVE"
    end

    if haskey(chrom, "MS2") == 1
        wf = 1
    elseif haskey(chrom, "MS2") == 0
        wf = 2
        println("The data does not include MS2 and it will processed only based on MS1!")
    else
        println("The data file is not a corrcet one!")
        return wf =3

    end

    if wf == 1


        pre_mz1 = Float64.(unique(round.(chrom["MS2"]["PrecursorIon"],digits=2)))
        pre_mz = Float64.(unique(round.(chrom["MS2"]["PrecursorIon"],digits=3)))
        prec_win = chrom["MS2"]["PrecursorIonWin"][1:length(pre_mz1)]
        tv=chrom["MS1"]["Mz_values"][:];
        ms1_win=maximum(chrom["MS1"]["Mz_values"][:])-minimum(tv[tv .> 0])

        if length(prec_win) > 1 && length(unique(prec_win)) >= 3 && minimum(prec_win) > 1 && length(chrom["MS2"]["PrecursorIon"])/length(pre_mz1) >= length(pre_mz1)


            #SWATH
            println("This file will be processed as a SWATH file.")
            table = screening_swath(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

        elseif length(unique(pre_mz1)) <= 3
            r_m2 = chrom["MS2"]["Rt"][2]
            ind_r_m1 = findfirst(x -> x >= r_m2,chrom["MS1"]["Rt"])
            t_rt = chrom["MS1"]["Rt"][ind_r_m1]
            t_rt2 = chrom["MS1"]["Rt"][ind_r_m1 + 1]
            cl_n = length(findall(x -> t_rt2 >= x >= t_rt,chrom["MS2"]["Rt"]))

            if cl_n == 1
                # DIA
                println("This file will be processed as a normal DIA file.")
                table = screening_dia(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

            elseif cl_n >1
                # DIA multi-collision
                println("This file will be processed as a multicollision one.")
                table = screening_mc(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)
            end

        elseif length(unique(pre_mz1)) > 3 && length(chrom["MS2"]["PrecursorIon"])/length(pre_mz1) < length(pre_mz1)

            # DDA
            println("This file will be processed as a DDA file.")
            table = screening_dda(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

        else
            println("This file type cannot be selected automatically. You need to process this file using the file specific function.")
        end

    elseif wf == 2
        println("This file will be processed only at MS1 level due to the lack of MS2 level.")
        table = screening_ms1(chrom,sus_list,mass_tol,min_int,rt_width,iso_depth,mode)

    end


    return table
end


################################################
#

"""
using MS_Import
include("/Users/saersamanipour/Desktop/dev/pkgs/ulsa.jl/src/PeakFind.jl")



#path2sus = "/Users/saersamanipour/Downloads/transfer_5508938_files_026e84ac/DataBase_standards.csv"
path2sus = "/Volumes/SAER HD/Data/Temp_files/Jake/Sus_screen/normanews2.csv"
mode = "POSITIVE" # "NEGATIVE" # 
source = "ESI"


# File import
pathin="/Users/saersamanipour/Desktop/UvA/tempfiles"
#pathin = "/Users/saersamanipour/Downloads/transfer_5508938_files_026e84ac"
filenames=["AIMSMS-positive-AI-2-01.mzXML"]
mz_thresh = [100,550]
int_thresh = 200

chrom=import_files(pathin,filenames,mz_thresh,int_thresh)

chrom["MS1"]["Mz_values"]

sus_e = suslist2entries(path2sus);
# sus_list = deepcopy(sus_e)
# println(sus_e)

# Suspect Screening
mass_tol = 0.05
min_int = 1000
iso_depth = 5
rt_width = 0.5

table = suspect_screening(chrom,sus_e,mass_tol,min_int,rt_width,iso_depth)

CSV.write("table_sus.csv",table)

"""
